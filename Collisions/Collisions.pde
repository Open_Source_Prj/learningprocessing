SquareMover squareMover = new SquareMover(); //<>// //<>//

void setup() {
  size(1080, 680);
  //fullScreen();

  background(255);
  frameRate(100);
}

void draw() {
  background(255);
  squareMover.update();
}

void mousePressed() {
  squareMover.mousePressed();
}

void mouseDragged() {
  squareMover.mouseDragged();
}
