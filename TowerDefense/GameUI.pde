class GameUI {
  GameStats gameStats;
  
  GameUI(GameStats gameStats) {
    this.gameStats = gameStats;
  }

  void update() {
  }

  void display() {
    textSize(30);
    String message = String.format("Credits: %d", gameStats.credits);
    text(message, 10, 30);
    
    String frame = String.format("FrameRate: %.2f", frameRate);
    text(frame, 10, 70);
  }
}
