GameManager game; //<>//

void setup() {
  size(1400, 1080);
  frameRate(60);
  background(0);
  game = new GameManager();
  game.initialize();
}

void draw() {
  //translate(width /2 , height / 2);
  background(0);

  //println(frameRate);
  //println(random(1));

  game.update();
  game.display();
}

void mousePressed() {
  //if (mouseButton == LEFT) {
  //  game.enemies.add(new EnemyProvider().getEnemy(game.tower.stats.position));
  //}
  int cost = 10;
  if (game.stats.credits >= cost) {
    if (mouseButton == LEFT) {
      game.tower.stats.damage++;
      println("Tower damage upgrade to " + game.tower.stats.damage);
    } else if (mouseButton == RIGHT) {
      game.tower.stats.fireRate++;
      println("Tower fireRate upgrade to " + game.tower.stats.fireRate);
    }
    game.stats.credits -= cost;
  } else {
    println("Not enough credits, required " + cost);
  }
}
