import java.util.Collections;
import java.util.HashSet;

class Tower {
  TowerStats stats;
  private float lastShotTime;
  private ArrayList<Enemy> enemiesInRange;
  private HashSet<Enemy> enemiesInRangeHash;


  Tower(TowerStats towerStats) {
    this.stats = towerStats;
    this.lastShotTime = 0;
    this.enemiesInRange = new ArrayList<Enemy>();
    this.enemiesInRangeHash = new HashSet<Enemy>();
  }

  void display() {
    noFill();
    stroke(255);
    strokeWeight(2);
    displayTower();
    displayRange();
  }

  void update() {
    sortEnemiesByDistance();
  }

  void addInRange(Enemy enemy) {
    if (enemiesInRangeHash.add(enemy)) {
      enemiesInRange.add(enemy);
    }
  }
  
  void removeEnemy(Enemy enemy) {
    enemiesInRange.remove(enemy);
    enemiesInRangeHash.remove(enemy);
    
  }

  void shoot(ArrayList<Projectile> projectileArray) {
    if (enemiesInRange.isEmpty()) {
      return;
    }

    Enemy enemyClosest = enemiesInRange.get(0);

    float currentTime = millis() / 1000.0;

    if (currentTime - lastShotTime >= 1.0 / stats.fireRate) {
      Projectile pj = new ProjectileProvider().getProjectile(stats.position, enemyClosest.stats.position, stats.damage);
      projectileArray.add(pj);

      lastShotTime = currentTime;
    }
  }

  void takeDamage(float damage) {
    stats.hp -= damage;
  }

  boolean isDead() {
    return stats.hp <= 0;
  }

  //TODO: can change to track faster enemies and remove and add at correct position instead of sorting entire collection
  private void sortEnemiesByDistance() {
    Collections.sort(enemiesInRange);
  }

  private void displayTower() {
    rectMode(CENTER);
    rect(stats.position.x, stats.position.y, stats.size, stats.size);
  }

  private void displayRange() {
    circle(stats.position.x, stats.position.y, stats.range * 2);
  }
}
