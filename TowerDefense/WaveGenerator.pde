class WaveGenerator {
  int currentWaveNumber;
  float spawnRate;
  int totalEnemies;
  PVector towerPosition;
  ArrayList<Enemy> enemies;
  IWave currentWave;
  float cooldownStartTime;
  boolean onCooldown = false;

  WaveGenerator(ArrayList<Enemy> enemies, PVector towerPosition) {
    this.towerPosition = towerPosition;
    this.enemies = enemies;
    this.spawnRate = 1000;
    this.totalEnemies = 10;
    this.currentWaveNumber = 1;

    currentWave = new Wave(totalEnemies, spawnRate, currentWaveNumber, towerPosition, enemies);
    //this.currentWave = new WaveStub(enemies, towerPosition);
  }

  void update() {
    if (currentWave.isOver() && !onCooldown) {
      onCooldown = true;
      cooldownStartTime = millis();
    }

    if (onCooldown) {
      if (millis() - cooldownStartTime >= 10000) {
        onCooldown = false;
        totalEnemies += currentWaveNumber;
        if (spawnRate > 0.5) {
          spawnRate -= 0.1;
        }
        currentWaveNumber++;
        currentWave = new Wave(totalEnemies, spawnRate, currentWaveNumber, towerPosition, enemies);
        println("starting new wave " + currentWaveNumber);
      }
    }
    
    currentWave.update();
  }
}

interface IWave {
  void update();
  boolean isOver();
}

class Wave implements IWave {
  EnemyProvider enemyProvider;
  int waveNumber;
  float duration;
  float spawnRate;
  int totalEnemies;
  int enemiesSpawned;
  float lastSpawnTime;
  PVector towerPosition;
  ArrayList<Enemy> enemies;

  Wave(int totalEnemies, float spawnRate, int waveNumber, PVector towerPosition, ArrayList<Enemy> enemies) {
    this.totalEnemies = totalEnemies;
    this.spawnRate = spawnRate;
    this.waveNumber = waveNumber;
    this.enemiesSpawned = 0;
    this.lastSpawnTime = 0;
    this.enemyProvider = new EnemyProvider();
    this.towerPosition = towerPosition;
    this.enemies = enemies;
  }

  void update() {
    if (millis() - lastSpawnTime > spawnRate && enemiesSpawned < totalEnemies) {
      float speed = 1 + waveNumber * 0.1;
      //float speed = 0 + millis() / 1000;
      int hp = 0 + waveNumber;
      int damage = 1;

      if (waveNumber % 3 == 0) {
        damage += waveNumber / 3;
      }

      enemies.add(enemyProvider.getEnemy(towerPosition, speed, hp, damage));
      enemiesSpawned++;
      lastSpawnTime = millis();
    }
  }

  boolean isOver() {
    return totalEnemies <= enemiesSpawned;
  }
}
