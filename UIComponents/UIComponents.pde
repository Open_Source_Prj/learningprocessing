import esy.gui.*;

Button btn;
Button btn2;
Button btn3;

TextBox tb;

EsyGui gui;

void setup() {
  size(1080, 720);

  gui = new EsyGui(this);

  //Component.Initialize(this);

  btn = new Button(new PVector(50, 50), 100, 50, "Buton 1", "Some text");
  btn2 = new Button(new PVector(200, 50), 100, 50, "Buton 2");
  btn2.setAnimation(false);
  btn3 = new Button(new PVector(200, 150), 100, 50, "Buton 3");

  tb = new TextBox(new PVector(50, 210), 200, 30, "TextBox 1", "Placeholder Text");

  btn.setOnClick(this::buttonClicked);
  btn.setOnMouseHover(this::buttonHover);
  btn.setOnMouseMove(this::buttonMove);
  btn.setOnMouseDrag(this::buttonDrag);
  btn.setOnMouseDown(this::buttonDown);
  btn.setOnMouseUp(this::buttonUp);
  btn.setOnMouseEnter(this::buttonEnter);
  btn.setOnMouseLeave(this::buttonLeave);

  btn2.setOnClick(this::buttonClicked);
  btn2.setOnKeyDown(this::buttonKeyDown);

  tb.setOnKeyType(this::tbKeyType);

  gui.addComponent(btn);
  gui.addComponent(btn2);
  gui.addComponent(btn3);
  gui.addComponent(tb);

  //btn.setTextAlign(CENTER, CENTER);
}

void draw() {
  background(240, 240, 240);

  gui.update();
  gui.display();
}

void buttonKeyDown(Object sender, KeyEventArgs e) {
  Button button = (Button) sender;

  if (e.keycode == 'S' && e.control && e.shift) {
    println("Control + Shift + S pressed.");
  } else {
    println(e.keychar + " " + e.keycode);
  }
}

void tbKeyType(Object sender, KeyTypeEventArgs e) {
  TextBox tb = (TextBox) sender;
  println("Pressed: " + e.keyChar + " on: " + tb.getName());
  
  if (e.keyChar == 'x') {
    e.handled = true;
  }
}

void buttonClicked(Object sender, EventArgs e) {
  Button button = (Button) sender;
  println("Button clicked: " + button.getName() + " " + button.getText());
}

void buttonHover(Object sender, EventArgs e) {
  Button button = (Button) sender;

  cursor(HAND);
}

void buttonMove(Object sender, MouseEventArgs e) {
  //println(e.x + " | " + e.y);
}

PVector offset;

void buttonDrag(Object sender, MouseEventArgs e) {
  Button button = (Button) sender;

  if (e.mouseBtn == LEFT) {
    button.getPosition().x = e.x + offset.x;
    button.getPosition().y = e.y + offset.y;
  }
}

void buttonDown(Object sender, MouseEventArgs e) {
  Button button = (Button) sender;
  offset = new PVector(button.getPosition().x - e.x, button.getPosition().y - e.y);
  println("Mouse pressed: " + e.mouseBtn);
}

void buttonUp(Object sender, MouseEventArgs e) {
  println("Mouse released: " + e.mouseBtn);
}

void buttonEnter(Object sender, EventArgs e) {
  println("Mouse Enter");
}

void buttonLeave(Object sender, EventArgs e) {
  println("Mouse Leave");
  cursor(ARROW);
}
