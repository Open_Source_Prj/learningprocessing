package esy.gui;

public class MouseEventArgs extends EventArgs {
  public float x;
  public float y;
  public int mouseBtn;

  public MouseEventArgs(float x, float y, int mouseBtn) {
    this.x = x;
    this.y = y;
    this.mouseBtn = mouseBtn;
  }
}
