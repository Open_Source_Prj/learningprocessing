Graph gph; //<>//
PVector rand;

void setup() {
  size(1280, 720);
  frameRate(120);
  gph = new Graph(50);
}
PVector clicked;

class Tuple{
  PVector first;
  PVector second;
  
  Tuple(PVector first, PVector second){
    this.first = first;
    this.second = second;
  }
}

int ct = 0;

ArrayList<Tuple> lines = new ArrayList<Tuple>();

void draw() {
  background(0);
  randomAdd();
  gph.show();

  if (rand != null) {
    fill(0, 255, 0);
    ellipse(rand.x, rand.y, 7, 7);
  }
  
  //if(ct < 4){
  //  lines.add(new Tuple(new PVector(random(width), random(height)), new PVector(random(width), random(height))));
  //  ct++;
  //}
  
  //for(Tuple line : lines){
  //  stroke(180);
  //  line(line.first.x, line.first.y, line.second.x, line.second.y);
  //}
  //if (mousePressed) {
  //  stroke(255);
  //  line(clicked.x, clicked.y, mouseX, mouseY);
  //}
}

void mousePressed() {
  clickedAddNode();
  clicked = new PVector(mouseX, mouseY);
}

void mouseReleased(){
  lines.add(new Tuple(clicked, new PVector(mouseX, mouseY)));
}

void clickedAddNode() {
  if (mouseButton == LEFT) {
    PVector mouse = new PVector(mouseX, mouseY);
    gph.addNode(mouse);
  } else if (mouseButton == RIGHT) {
    rand = new PVector(random(width), random(height));
    gph.addNode(rand);
  } else if (mouseButton == CENTER) {
    gph.reset();
    loop();
  }
}

void randomAdd() {
  //if (!gph.targetReached) {
  //  rand = new PVector(random(width), random(height));
  //  gph.addNode(rand);
  //}
  rand = new PVector(random(width), random(height));
  gph.addNode(rand);
  if (gph.targetReached) {
    noLoop();
    //gph.reset();
  }
}

//void addRandomNode() {
//  rand = new PVector(random(width), random(height));
//  addNode(rand);
//  fill(0, 255, 0);
//  ellipse(rand.x, rand.y, 7, 7);
//}

//void addNode(PVector pos) {
//  if (nodes.size() == 0) {
//    nodes.add(new Node(pos));
//  } else {
//    Node closest = getClosest(pos);
//    PVector cls = new PVector(closest.pos.x, closest.pos.y);
//    PVector temp = new PVector(cls.x, cls.y);
//    temp.sub(pos);
//    temp.limit(step);
//    cls.sub(temp);

//    Node newNode = new Node(cls);
//    closest.AddToSelf(newNode);
//    nodes.add(newNode);
//  }
//}

//Node getClosest(PVector toThis) {
//  float dst = width*height;
//  Node result = new Node(toThis);
//  for (Node n : nodes) {
//    float dstN = dist(n.pos.x, n.pos.y, toThis.x, toThis.y);
//    if (dstN < dst) {
//      dst = dstN;
//      result = n;
//    }
//  }

//  return result;
//}

//void classic() {
//  if (array.size() > 0) {
//    PVector temp = array.get(0);
//    ellipse(temp.x, temp.y, 10, 10);
//    for (int i = 1; i < array.size(); i++) {
//      PVector act = array.get(i);
//      PVector prev = array.get(i - 1);

//      ellipse(act.x, act.y, 10, 10);
//      line(prev.x, prev.y, act.x, act.y);
//    }
//  }
//  if (rand != null) {
//    fill(0, 255, 0);
//    ellipse(rand.x, rand.y, 7, 7);
//  }
//  fill(255, 0, 0);
//  ellipse(mouseX, mouseY, 5, 5);
//}

//void add() {
//  rand = new PVector(random(width), random(height));
//  if (array.size() == 0) {
//    array.add(rand);
//  } else {
//    PVector a = array.get(array.size() - 1);

//    float dst = dist(a.x, a.y, rand.x, rand.y);
//    if (dst < step) {
//      array.add(rand);
//    } else {
//      float ma = (step*100)/dst;
//      float mb = 100-ma;
//      float t = -ma/mb;

//      float xm = (a.x - t*rand.x) / (1-t);
//      float ym = (a.y - t*rand.y) / (1-t);

//      PVector c = new PVector(xm, ym);

//      array.add(c);
//      println(array.size());
//    }
//  }
//}

//void clickedCode() {
//  if (mouseButton == LEFT) {
//    if (array.size() == 0) {
//      array.add(new PVector(mouseX, mouseY));
//    } else {
//      PVector mouse = new PVector(mouseX, mouseY);
//      PVector arrLast = arrayClosest(mouse);
//      PVector start = new PVector(arrLast.x, arrLast.y);
//      PVector temp = new PVector(start.x, start.y);
//      //temp.sub(mouse);
//      //temp.limit(50);
//      //start.sub(temp);

//      temp.sub(mouse);
//      temp.limit(step);
//      start.sub(temp);

//      array.add(start);
//    }
//  } else if (mouseButton == RIGHT) {
//    rand = new PVector(random(width), random(height));
//    if (array.size() == 0) {
//      array.add(rand);
//    } else {
//      PVector arrLast = array.get(array.size() - 1);
//      PVector start = new PVector(arrLast.x, arrLast.y);
//      PVector temp = new PVector(start.x, start.y);
//      temp.sub(rand);
//      temp.limit(50);
//      start.sub(temp);

//      array.add(start);
//    }
//  } else if (mouseButton == CENTER) {
//    array.clear();
//  }
//}

//PVector arrayClosest(PVector clicked) {
//  float dst = width*height;
//  PVector result = new PVector(clicked.x, clicked.y);
//  for (PVector v : array) {
//    float dstN = dist(v.x, v.y, clicked.x, clicked.y);
//    if (dstN < dst) {
//      dst = dstN;
//      result = new PVector(v.x, v.y);
//    }
//  }
//  //println(result);
//  return result;
//}
