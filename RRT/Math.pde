static class Math {

  /**
   * Check collision between a line and a circle
   * @param a First point of the line
   * @param b Second point of the line
   * @param c Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the line is colliding with the circle
   */
  static boolean collisionLineCircle(PVector a, PVector b, PVector c, float r) {
    return collisionLineCircle(a.x, a.y, b.x, b.y, c.x, c.y, r);
  }


  /**
   * Check collision between a line and a circle
   * @param x1,y1 First point of the line
   * @param x2,y2 Second point of the line
   * @param cx,cy Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the line is colliding with the circle
   */
  static boolean collisionLineCircle(float x1, float y1, float x2, float y2, float cx, float cy, float r) {
    // is either end INSIDE the circle?
    // if so, return true immediately
    boolean inside1 = collisionPointCircle(x1, y1, cx, cy, r);
    boolean inside2 = collisionPointCircle(x2, y2, cx, cy, r);
    if (inside1 || inside2) return true;

    // get length of the line
    float distX = x1 - x2;
    float distY = y1 - y2;
    float len = sqrt( (distX*distX) + (distY*distY) );

    // get dot product of the line and circle
    float dot = ( ((cx-x1)*(x2-x1)) + ((cy-y1)*(y2-y1)) ) / pow(len, 2);

    // find the closest point on the line
    float closestX = x1 + (dot * (x2-x1));
    float closestY = y1 + (dot * (y2-y1));

    // is this point actually on the line segment?
    // if so keep going, but if not, return false
    boolean onSegment = collisionLinePoint(x1, y1, x2, y2, closestX, closestY);
    if (!onSegment) return false;

    // optionally, draw a circle at the closest
    // point on the line
    //fill(255,0,0);
    //noStroke();
    //ellipse(closestX, closestY, 20, 20);

    // get distance to closest point
    distX = closestX - cx;
    distY = closestY - cy;
    float distance = sqrt( (distX*distX) + (distY*distY) );

    if (distance <= r) {
      return true;
    }
    return false;
  }


  /**
   * Check collision between a point and a circle
   * @param p Point position
   * @param c Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the point is colliding with the circle
   */
  static boolean collisionPointCircle(PVector p, PVector c, float r) {
    return collisionPointCircle(p.x, p.y, c.x, c.y, r);
  }

  /**
   * Check collision between a point and a circle
   * @param px,py Point position
   * @param cx,cy Circle's center position
   * @param r Radius of the circle
   * @return boolean Returns true if the point is colliding with the circle
   */
  static boolean collisionPointCircle(float px, float py, float cx, float cy, float r) {

    // get distance between the point and circle's center
    // using the Pythagorean Theorem
    float distX = px - cx;
    float distY = py - cy;
    float distance = sqrt((distX*distX) + (distY*distY));

    // if the distance is less than the circle's
    // radius the point is inside!
    if (distance <= r) {
      return true;
    }
    return false;
  }


  /**
   * Check collision between a line and a point
   * @param a First point of the line
   * @param b Second point of the line
   * @param p Point position
   * @return boolean Returns true if the line is colliding with the point
   */
  static boolean collisionLinePoint(PVector a, PVector b, PVector p) {
    return collisionLinePoint(a.x, a.y, b.x, b.y, p.x, p.y);
  }


  /**
   * Check collision between a line and a point
   * @param x1,y1 First point of the line
   * @param x2,y2 Second point of the line
   * @param px,py The point to check against
   * @return boolean Returns true if the line is colliding with the point
   */
  static boolean collisionLinePoint(float x1, float y1, float x2, float y2, float px, float py) {

    // get distance from the point to the two ends of the line
    float d1 = dist(px, py, x1, y1);
    float d2 = dist(px, py, x2, y2);

    // get the length of the line
    float lineLen = dist(x1, y1, x2, y2);

    // since floats are so minutely accurate, add
    // a little buffer zone that will give collision
    float buffer = 0.1;    // higher # = less accurate

    // if the two distances are equal to the line's
    // length, the point is on the line!
    // note we use the buffer here to give a range,
    // rather than one #
    if (d1+d2 >= lineLen-buffer && d1+d2 <= lineLen+buffer) {
      return true;
    }
    return false;
  }
}
