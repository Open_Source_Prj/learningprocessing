class Projectile{
  final int _width = 6;
  final int _height = 6;
  
  final float speed = 5;
  
  PVector location;
  
  Projectile(float playerX, float playerY){
    location = new PVector(playerX, playerY);
  }
  
  void show(){
    display();
    update();
  }
  
  private void display(){
    noStroke();
    fill(0);
    rect(location.x, location.y, _width, _height);
  }
  
  private void update(){
    location.y -= speed;
  }
}
