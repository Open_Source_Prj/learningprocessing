class Player{
  final float size = 50;
  
  float xPos;
  float yPos;
  
  Player(float x, float y){
    xPos = x;
    yPos = y;
  }
  
  void show(){
    display();
    update();
  }
  
  private void display(){
    strokeWeight(1);
    fill(0, 0, 255);
    triangle(xPos, yPos, xPos + size / 2, yPos + size, xPos - size / 2, yPos + size);
  }
  
  private void update(){
    xPos = mouseX;
  }
  
  Projectile shoot(){
    return new Projectile(xPos, yPos);
  }
}
