World game;

void setup() {
  size(1280, 720);
  background(255);
  game = new World(1280, 720);
  frameRate(1000);
  thread("shootingThread");
}

//TODO: Adjust ball speed acording to size
//TODO: Adjust ball color acording to size

int col = 0;
Timer tm = new Timer();
void draw() {
  game.run();
  println(tm.elapsedSeconds());
}


void shootingThread() {
  game.playerShoot();
}
