class World {
  final int shootInterval = 700;
  final int timeCount = 15;
  int maxUnits = 1;
  int score = 1;
  float speedFactor = 1;
  boolean leveledUp = false;

  float groundPos;

  float screenHeight;
  float screenWidth;

  ArrayList<Asteroid> asteroids = new ArrayList<Asteroid>();
  ArrayList<Projectile> projectiles = new ArrayList<Projectile>();
  Player player;
  Timer timer;

  World(float screenWidth, float screenHeight) {
    groundPos = screenHeight - 100;
    this.screenHeight = screenHeight;
    this.screenWidth = screenWidth;

    player = new Player(groundPos + 50, screenWidth / 2);
    timer = new Timer();
  }

  //TODO: Improve loops so the asteroid kill will be instant

  void run() {
    background(255);
    drawGround();
    checkAsteroidProjectileCollision();
    display();
    update();
    displayProjectiles();
    removeProjectiles();
    //checkGameOver();
    increaseLevel();
  }

  private void display() {
    for (int i = 0; i < asteroids.size(); ++i) {
      Asteroid asteroid = asteroids.get(i);
      asteroid.show();
      if (!inBounds(asteroid.getY() + asteroid.size / 2)) {
        asteroids.remove(asteroid);
      }
    }
    player.show();
  }

  void playerShoot() {
    while (true) {
      projectiles.add(player.shoot());
      delay(shootInterval);
    }
  }

  private void update() {
    if (!isArrayFilled()) {
      asteroids.add(new Asteroid(random(screenWidth), speedFactor));
    }
  }

  private boolean inBounds(float y) {
    return y < groundPos;
  }

  private void drawGround() {
    drawGroundLine();
    drawGrass();
  }

  private void drawGroundLine() {
    float yPos = groundPos;
    stroke(2);
    strokeWeight(3);
    line(0, yPos, screenWidth, yPos);
  }

  private void drawGrass() {
    float yPos = groundPos + 2;
    noStroke();
    fill(0, 255, 0);
    rect(0, yPos, screenWidth, groundPos);
  }

  private boolean isArrayFilled() {
    return asteroids.size() == maxUnits;
  }

  private void displayProjectiles() {
    for (int i = 0; i < projectiles.size(); ++i) {
      Projectile pj = projectiles.get(i);
      pj.show();
    }
  }

  void removeProjectiles() {
    for (int i = 0; i < projectiles.size(); ++i) {
      if (projectiles.get(i).location.y <= 0) {
        projectiles.remove(projectiles.get(i));
      }
    }
  }

  private void checkAsteroidProjectileCollision() {
    for (int i = 0; i < asteroids.size(); ++i) {
      Asteroid asteroid = asteroids.get(i);
      for (int j = 0; j < projectiles.size(); ++j) {
        Projectile projectile = projectiles.get(j);
        if (((asteroid.location.y + asteroid.size / 2) >= projectile.location.y) && 
          ((projectile.location.x - asteroid.size / 2) < asteroid.location.x) &&
          ((projectile.location.x + asteroid.size / 2) > asteroid.location.x)) {
          ++score;
          Projectile pj = projectiles.get(j);
          projectiles.remove(pj);
          asteroid.hit();
          asteroid.boost(30 * ((int)random(2) * 2 - 1));
          if (asteroid.isDead()) {
            asteroids.remove(asteroid);
          }
        }
        //float distance = dist(projectile.location.x, projectile.location.y, asteroid.location.x, asteroid.location.y);

        //if(distance <= asteroid.size){
        //  asteroids.remove(asteroid); 
        //}
      }
    }
  }

  private void checkGameOver() {
    for (Asteroid asteroid : asteroids) {
      if ((asteroid.location.y + asteroid.size / 2) > groundPos) {
        noLoop();
        //background(255);
        //display();
        //drawGround();
        textSize(64);
        text("Game over", screenWidth / 2, screenHeight / 2);
        fill(0);
      }
    }
  }

  private void increaseLevel() {
    if ((timer.elapsedSeconds() % timeCount == 0) && !leveledUp) {
      ++maxUnits;
      speedFactor +=1;
      leveledUp = true;
    } else if (timer.elapsedSeconds() % timeCount != 0) {
      leveledUp = false;
    }
  }
}
