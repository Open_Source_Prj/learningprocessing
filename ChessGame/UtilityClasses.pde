class Position {
  public float x;
  public float y;

  public Position(float _x, float _y) {
    x = _x;
    y = _y;
  }
}

class TextWriter {
  private final String FONT_NAME_DEFAULT = "Arial Bold";
  private final float FONT_SIZE_DEFAULT = 18;
  
  private PFont _font;
  private color _col;

  public TextWriter() {
    _font = createFont(FONT_NAME_DEFAULT, FONT_SIZE_DEFAULT);
    _col = color(0);
  }

  public TextWriter(float size){
    _font = createFont(FONT_NAME_DEFAULT, size);
    _col = color(0);
  }
  
  public TextWriter(float size, color col){
    _font = createFont(FONT_NAME_DEFAULT, size);
    _col = col;
  }

  public TextWriter(PFont font) {
    this(font, color(0));
  }

  public TextWriter(PFont font, color col) {
    _font = font;
    _col = col;
  }

  public void write(String txt, float x, float y) {
    write(txt, x, y, _col, _font);
  }

  public void write(String txt, float x, float y, color col) {
    write(txt, x, y, col, _font);
  }

  public void write(String txt, float x, float y, color col, PFont font) {
    textAlign(CENTER, CENTER);
    textFont(font);
    fill(col);
    text(txt, x, y);
  }
  
  public void setSize(float size){
    _font = createFont(_font.getName(), size);
  }
  
  public void setFont(PFont font){
    _font = font;
  }
  
  public void setColor(color col){
    _col = col;
  }
  
  
  
  
  
  
  
  
  
  
  
}
