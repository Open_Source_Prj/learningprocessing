class Board {
  private final int TILE_SIZE = 100;
  private final int BOARD_SIZE = 8;
  private final Position BOARD_POSITION = new Position(50, 50);

  private TextWriter _tw;


  Tile[][] _tiles = new Tile[BOARD_SIZE][BOARD_SIZE];


  public Board() {
    float sz = 0.3 * TILE_SIZE;
    _tw = new TextWriter(sz);
    
    initializeBoard();
  }

  public void display() {
    showBoard();
    showBoardNumbers();
    showBoardLetters();
  }

  private void showBoard() {
    pushMatrix();
    translate(BOARD_POSITION.x, BOARD_POSITION.y);

    for(int i = 0; i < BOARD_SIZE; ++i){
      for(int j = 0; j < BOARD_SIZE; ++j){
        _tiles[i][j].display();
      }
    }

    popMatrix();
  }

  private void showBoardNumbers() {
    float offset = 0.1 * TILE_SIZE;
    float xPos = BOARD_POSITION.x + offset;
    float yPos = BOARD_POSITION.y + TILE_SIZE / 2;
    color c;

    for (int i = BOARD_SIZE; i > 0; --i) {
      c = (i % 2 == 0) ? WHITE : BROWN;

      _tw.write(Integer.toString(i), xPos, yPos, c);
      yPos += TILE_SIZE;
    }
  }

  private void showBoardLetters() {
    float offset = 0.2 * TILE_SIZE;
    float xPos = BOARD_POSITION.x + TILE_SIZE / 2;
    float yPos = (BOARD_POSITION.y - offset) + BOARD_SIZE * TILE_SIZE;
    color c;

    for (int i = 0; i < BOARD_SIZE; ++i) {
      c = (i % 2 == 0) ? BROWN : WHITE;

      String letter = String.valueOf((char)(97 + i));

      _tw.write(letter, xPos, yPos, c);
      xPos += TILE_SIZE;
    }
  }

  private void initializeBoard() {
    for (int i = 0; i < BOARD_SIZE; ++i) {
      for (int j = 0; j < BOARD_SIZE; ++j) {
        if ((i + j) % 2 == 0) {
          _tiles[i][j] = new Tile(i, j, i * TILE_SIZE, j * TILE_SIZE, TILE_SIZE, BROWN);
        } else {
          _tiles[i][j] = new Tile(i, j, i * TILE_SIZE, j * TILE_SIZE, TILE_SIZE, WHITE);
        }
      }
    }
  }
}
